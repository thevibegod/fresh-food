from django.db import models
from executives.models import DeliveryMan
import random
from django.utils import timezone as tz


# Create your models here.
class Address(models.Model):
    door_no = models.CharField(max_length=10)
    street = models.CharField(max_length=50)
    area = models.CharField(max_length=20)
    pincode = models.CharField(max_length=7)
    landmark = models.CharField(max_length=50)
    lat = models.DecimalField(decimal_places=7, max_digits=12)
    long = models.DecimalField(decimal_places=7, max_digits=12)


class Order(models.Model):
    INITIATED = 0
    ASSIGNED = 1
    DELIVERED = 2
    CANCELLED = 3
    STATUS_CHOICES = (
        (INITIATED, "Initiated"), (ASSIGNED, "Assigned"), (DELIVERED, "Delivered"), (CANCELLED, "Cancelled"))
    name = models.CharField(max_length=40)
    phone = models.CharField(max_length=10)
    assigned_to = models.ForeignKey(DeliveryMan, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    address = models.OneToOneField(Address, on_delete=models.CASCADE)
    status = models.IntegerField(default=INITIATED, choices=STATUS_CHOICES)
    pin = models.IntegerField(default=random.randint(1000, 9999))
    placed_at = models.DateTimeField(default=tz.now)
    price = models.DecimalField(decimal_places=2, max_digits=7)

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(price__gt='0'), name='price_order_gt_0'),
        ]


class Package(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()
    price = models.DecimalField(decimal_places=2, max_digits=7)
    weight = models.DecimalField(decimal_places=2, max_digits=5)
    image = models.ImageField(upload_to="packages/")
    stock = models.IntegerField(default=0)
    stock_updated_at = models.DateTimeField(default=None, null=True, blank=True)

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(price__gt='0'), name='price_package_gt_0'),
            models.CheckConstraint(check=models.Q(weight__gt='0'), name='weight_gt_0'),
            models.CheckConstraint(check=models.Q(stock__gt='-1'), name='stock_gt_0'),
        ]


class LineItems(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    package = models.ForeignKey(Package, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(quantity__gt='0'), name='quantity_gt_0'),
        ]
