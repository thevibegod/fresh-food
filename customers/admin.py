from django.contrib import admin
from .models import Address, Order, Package, LineItems

# Register your models here.

admin.site.register(Address)
admin.site.register(Order)
admin.site.register(Package)
admin.site.register(LineItems)