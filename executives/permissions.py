def check_super_user(user):
    return user.is_authenticated and user.is_superuser


def check_sub_admin(user):
    return (user.is_authenticated and user.groups.filter(name="Sub Admin").exists()) or check_super_user(user)


def check_delivery_executive(user):
    return user.is_authenticated and hasattr(user, 'deliveryman')


def check_authenticated(user):
    return user.is_authenticated
