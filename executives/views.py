from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from .permissions import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from .models import *
from django.contrib.auth.models import Group
from django.views.generic import ListView
from django.http import JsonResponse
from django.utils import timezone as tz
from .forms import PackageCreationForm
from customers.models import *


# Create your views here.

@user_passes_test(check_authenticated, login_url='/executives/')
def executive_wait(request):
    user = request.user
    if check_sub_admin(user):
        return redirect('sub_admin-dashboard')
    elif check_delivery_executive(user):
        return redirect('delivery_exec-dashboard')
    return render(request, 'executive_wait.html')


@user_passes_test(check_authenticated, login_url='/executives/')
def auth_complete(request):
    # Get method checks and redirects to specific pages
    if request.method == 'GET':
        user = request.user
        print(user.password)
        if user.has_usable_password():
            if check_sub_admin(user):
                return redirect('order-assignment')
            elif check_delivery_executive(user):
                return redirect('current-delivery-executive-orders')
            return redirect(executive_wait)
        else:
            return render(request, 'executive_details_collect.html')
    # Post method processes detail-collection form inputs
    elif request.method == 'POST':
        password1, password2 = request.POST.get('password1'), request.POST.get('password2')
        if len(password1) < 7 or len(password2) < 7:
            messages.error(request, 'Please enter a valid password')
            return render(request, 'executive_details_collect.html')
        elif password1 != password2:
            messages.error(request, 'Passwords do not match. Try again')
            return render(request, 'executive_details_collect.html')
        else:
            user = request.user
            if user.has_usable_password():
                messages.warning(request, 'You have registered already. Notify the main Admin to add you')
            else:
                try:
                    user.set_password(password1)
                    user.save()
                    messages.success(request, 'All set. Notify the admin to authorize you.')
                    return redirect(executive_wait)
                except Exception as e:
                    messages.error(request, 'Something has gone wrong.' + e)


def executive_login_view(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            if len(request.POST['password']) < 6:
                messages.error(request, 'Password Invalid')
                return redirect('executive-base')
            try:
                email = request.POST['email']
                user = User.objects.get(email=email)
            except Exception as e:
                messages.error(request, 'Email does not exist')
                return redirect('executive-base')
            authenticated_user = authenticate(username=user.username, password=request.POST['password'])
            if authenticated_user is not None:
                login(request, authenticated_user)
                messages.success(request, 'You have been successfully logged in')
                return redirect(auth_complete)
    else:
        return redirect('/executives/')


@user_passes_test(check_super_user, login_url='/executives/')
def add_new_delivery_executive(request):
    # Render a page where the user is in a dropdown
    if request.method == 'GET':
        return render(request, 'add_delivery_executive.html')
    # Accept id of the deliveryman and create a DeliveryMan object.
    elif request.method == 'POST':
        delivery_man_email = request.POST['email']
        delivery_man_phone = request.POST['phone']
        delivery_man_vehicle_no = request.POST['vehicle_no']
        try:
            associated_user = User.objects.get(email=delivery_man_email)
        except Exception as e:
            messages.error(request,
                           'Such an email does not exist in the system. Ask the delivery man to login/register with the email')
            return redirect(add_new_delivery_executive)

        if check_sub_admin(associated_user):
            messages.error(request, 'The requested email already is a sub admin account.')
            return redirect(add_new_delivery_executive)

        if check_delivery_executive(associated_user):
            messages.warning(request, 'Delivery man with the entered email already exists')
            return redirect(add_new_delivery_executive)
        try:
            delivery_man_object = DeliveryMan.objects.create(user=associated_user, phone=delivery_man_phone,
                                                             vehicle_number=delivery_man_vehicle_no)
            messages.success(request,
                             f'Delivery executive added successfully. Email:{associated_user.email} .Kindly ask the person to login ')
            return redirect(add_new_delivery_executive)
        except Exception as e:
            messages.error(request, 'Something went wrong.. Please try later')
            return redirect(add_new_delivery_executive)


@user_passes_test(check_super_user, login_url='/executives/')
def add_sub_admin(request):
    if request.method == 'GET':
        return render(request, 'add_sub_admin.html')
    elif request.method == 'POST':
        email = request.POST['email']
        try:
            admin_user = User.objects.get(email=email)
            if check_delivery_executive(admin_user):
                messages.error(request, 'The given email is already a delivery man account')
                return redirect(add_sub_admin)
            if check_sub_admin(admin_user):
                messages.warning(request, 'Admin user with the email already exists')
                return redirect(add_sub_admin)
            else:
                admin_user.groups.add(Group.objects.get(name='Sub Admin'))
                messages.success(request, f'Sub Admin added successfully. Email:{admin_user.email}')
                return redirect(add_sub_admin)
        except Exception as e:
            print(e)
            messages.error(request, 'There is no user with such email')
            return redirect(add_sub_admin)


class TotalOrdersList(ListView):
    model = Order
    template_name = 'list_all_orders.html'
    context_object_name = 'order_list'

    def get_queryset(self):
        query_options = {
            'Initiated': Order.INITIATED,
            'Assigned': Order.ASSIGNED,
            'Delivered': Order.DELIVERED,
            'Cancelled': Order.CANCELLED
        }
        query = self.request.GET.get('query', None)
        if query is not None:
            query_set = Order.objects.filter(status=query_options[query])
        else:
            query_set = Order.objects.all()

        return query_set


@user_passes_test(check_delivery_executive)
def get_orders_for_delivery_executive(request):
    if request.method == 'GET':
        delivery_executive = request.user.deliveryman
        # It returns the orders for the current day (unsettled/pending orders) Model reference : status 0 or 1 which
        # explains the lte query.
        assigned_orders = Order.objects.filter(assigned_to=delivery_executive, status__lte=Order.ASSIGNED)
        context = {
            'assigned_orders': assigned_orders
        }
        return render(request, 'delivery_man_orders.html', context)


@user_passes_test(check_delivery_executive)
def confirm_pin(request):
    if request.method == 'GET':
        pin, order_id = request.GET.get('pin'), request.GET.get('order_id')
        try:
            order_object = Order.objects.get(id=order_id)
            if order_object.assigned_to == request.user.deliveryman:
                if order_object.pin == int(pin):
                    order_object.status = Order.DELIVERED
                    order_object.save()
                    return JsonResponse({
                        'code': 'SUCCESS',
                        'message': f"Order status successfully updated. Delivered to {order_object.name}"
                    })
                else:
                    print('you are searching the right thing')
                    return JsonResponse({
                        'code': 'ERROR',
                        'message': 'PIN is incorrect'
                    })
            else:
                return JsonResponse({
                    'code': 'ERROR',
                    'message': 'The order was assigned to a different delivery executive.'
                })
        except Exception as e:
            return JsonResponse({
                'CODE': "ERROR",
                'message': 'Sorry. There is no such order.'
            })


@user_passes_test(check_sub_admin)
def order_assignment(request):
    if request.method == "GET":
        try:
            delivery_men = DeliveryMan.objects.all()
            orders_for_the_day = Order.objects.filter(placed_at__contains=tz.now().date())
            print(orders_for_the_day)
            related_pincodes = request.user.zone.area_set.values_list('pincode', flat=True)
            print(related_pincodes)
            related_orders = orders_for_the_day.filter(address__pincode__in=related_pincodes, status=Order.INITIATED)
            print(related_orders)
            context = {
                'orders': related_orders,
                'delivery_men': delivery_men
            }
            return render(request, 'order_assignment.html', context)
        except Exception as e:
            print(e)
            return HttpResponse("there was some error in processing the data.")
        pass
    elif request.method == "POST":
        ids = request.POST['order_ids']
        assign_del_man_id = int(request.POST['assign_to'])
        try:
            delivery_executive = DeliveryMan.objects.get(id=assign_del_man_id)
        except Exception as e:
            print(e)
            return JsonResponse({
                'code': "ERROR",
                'message': "No such delivery executive."
            })
        ids = list(map(int, ids.split(',')))
        try:
            for id in ids:
                order_object = Order.objects.get(id=id)
                order_object.assigned_to = delivery_executive if delivery_executive else None
                order_object.status = Order.ASSIGNED
                order_object.save()
            return JsonResponse({
                'code': "SUCCESS",
                'message': "Successfully assigned orders."
            })
        except Exception as e:
            return JsonResponse({
                'code': "ERROR",
                'message': "No such delivery executive."
            })


@user_passes_test(check_super_user)
def add_package(request):
    if request.method == 'GET':
        form = PackageCreationForm()
        context = {
            'form': form
        }
        return render(request, 'add_package.html', context=context)
    elif request.method == 'POST':
        form = PackageCreationForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                package_commit_less = form.save(commit=False)
                package_object = Package(
                    name=package_commit_less.name,
                    description=package_commit_less.description,
                    price=package_commit_less.price,
                    weight=package_commit_less.weight,
                    image=package_commit_less.image,
                    stock=package_commit_less.stock,
                    stock_updated_at=tz.now()
                )
                package_object.save()
                context = {
                    'form': PackageCreationForm()
                }
                messages.success(request, 'Package successfully saved')
                return render(request, "add_package.html", context=context)
            except Exception as e:
                messages.error(request, 'Oops. Error saving the package.')
                context = {
                    'form': form
                }
                return render(request, 'add_package.html', context=context)
        else:
            context = {
                'form': form
            }
            return render(request, 'add_package.html', context=context)


@user_passes_test(check_super_user)
def list_all_packages(request):
    if request.method == "GET":
        try:
            packages = Package.objects.all()
            context = {
                'packages': packages
            }
            return render(request, 'list_packages.html', context=context)
        except Exception as e:
            print(e)
        return redirect('executive-base')


@user_passes_test(check_super_user)
def update_package(request, id):
    if request.method == "GET":
        pass
    elif request.method == "POST":
        pass
