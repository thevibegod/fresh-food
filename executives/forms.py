from django import forms
from customers.models import *


class PackageCreationForm(forms.ModelForm):
    class Meta:
        model = Package
        exclude = ['stock_updated_at']

    def __init__(self, *args, **kwargs):
        super(PackageCreationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if type(visible.field.widget) != forms.ClearableFileInput:
                visible.field.widget.attrs['class'] = 'form-control'

