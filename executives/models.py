from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Zone(models.Model):
    name = models.CharField(max_length=10)
    admin_responsible = models.OneToOneField(User, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)


class Area(models.Model):
    zone = models.ForeignKey(Zone, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    pincode = models.CharField(max_length=7)
    name = models.CharField(max_length=20)


class DeliveryMan(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=10)
    vehicle_number = models.CharField(max_length=20,unique=True)