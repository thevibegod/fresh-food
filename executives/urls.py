from django.urls import path
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    path('', TemplateView.as_view(template_name='executives_landing.html'), name='executive-base'),
    path('auth-complete', views.auth_complete, name='auth-complete'),
    path('executive/executive-wait', views.executive_wait, name='executive-wait'),
    path('login/', views.executive_login_view, name='executive-login'),
    path('add_delivery_executive/', views.add_new_delivery_executive, name='add-delivery-executive'),
    path('add_sub_admin/', views.add_sub_admin, name='add-sub-admin'),
    path('list_orders/', views.TotalOrdersList.as_view(), name='admin-order-list'),
    path('list_packages/', views.list_all_packages, name='list-package'),
    path('add_package/', views.add_package, name='add-package'),
    path('current_delivery_executive_orders/', views.get_orders_for_delivery_executive,
         name='current-delivery-executive-orders'),
    path('confirm_pin_and_mark_delivered/', views.confirm_pin, name='confirm-pin'),
    path('assign_orders/', views.order_assignment, name='order-assignment')
]
